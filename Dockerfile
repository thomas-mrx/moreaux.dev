FROM php:8.0-apache

RUN apt-get update
RUN apt-get install unzip -y

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer
COPY composer.json composer.json
COPY composer.lock composer.lock
RUN composer install --no-dev


COPY . /var/www/html/

RUN a2enmod headers
RUN a2enmod rewrite

CMD ["apache2-foreground"]
EXPOSE 80
