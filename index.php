<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Thomas @moreaux.dev</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" type="image/png" href="images/logo.png">

    <!-- STYLESHEETS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/all.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/simple-line-icons.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/slick.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/animate.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Preloader -->
    <div id="preloader" class="light">
        <div class="outer">
            <!-- Google Chrome -->
            <div class="infinityChrome">
                <div></div>
                <div></div>
                <div></div>
            </div>

            <!-- Safari and others -->
            <div class="infinity">
                <div>
                    <span></span>
                </div>
                <div>
                    <span></span>
                </div>
                <div>
                    <span></span>
                </div>
            </div>
            <!-- Stuff -->
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="goo-outer">
                <defs>
                    <filter id="goo">
                        <feGaussianBlur in="SourceGraphic" stdDeviation="6" result="blur" />
                        <feColorMatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />
                        <feBlend in="SourceGraphic" in2="goo" />
                    </filter>
                </defs>
            </svg>
        </div>
    </div>

    <!-- mobile header -->
    <header class="mobile-header-2">
        <div class="container">
            <!-- menu icon -->
            <div class="menu-icon d-inline-flex me-4">
                <button>
                    <span></span>
                </button>
            </div>
            <!-- logo image -->
            <div class="site-logo">
                <a href="/">
                    <img src="images/logo-light.png" alt="TM" />
                </a>
            </div>
        </div>
    </header>

    <!-- desktop header -->
    <header class="desktop-header-2 light d-flex align-items-start flex-column">

        <!-- logo image -->
        <div class="site-logo">
            <a href="/">
                <img src="images/logo.png" alt="TM" />
            </a>
        </div>

        <!-- main menu -->
        <nav>
            <ul class="vertical-menu scrollspy">
                <li class="active"><a href="#home"><i class="icon-home"></i></a></li>
                <li><a href="#about"><i class="icon-user-following"></i></a></li>
                <li><a href="#skills"><i class="icon-briefcase"></i></a></li>
                <li><a href="#experience"><i class="icon-graduation"></i></a></li>
                <li><a href="#works"><i class="icon-layers"></i></a></li>
                <li><a href="#contact"><i class="icon-bubbles"></i></a></li>
            </ul>
        </nav>

        <!-- site footer -->
        <div class="footer">
            <!-- copyright text -->
            <span class="copyright">© <?= date("Y") ?> moreaux.dev</span>
        </div>

    </header>

    <!-- main layout -->
    <main class="content-2">

        <!-- section home -->
        <section id="home" class="home d-flex light align-items-center">
            <div class="container">

                <!-- intro -->
                <div class="intro">
                    <!-- avatar image -->
                    <img src="images/me.jpeg" alt="Me" class="mb-4 w-25 circle shadow-dark" />

                    <!-- info -->
                    <h1 class="mb-2 mt-0">Thomas Moreaux</h1>
                    <span>I'm a <span class="text-rotating">Full Stack Developer, Self-Taught Pianist, Photography Lover, Music Enthusiast</span></span>

                    <!-- social icons -->
                    <ul class="social-icons light list-inline mb-0 mt-4">
                        <li class="list-inline-item"><a href="https://www.linkedin.com/in/thomas-moreaux/"><i class="fab fa-linkedin-in"></i></a></li>
                        <li class="list-inline-item"><a href="https://github.com/thomas-mrx"><i class="fab fa-github"></i></a></li>
                        <li class="list-inline-item"><a href="https://gitlab.com/thomas-mrx"><i class="fab fa-gitlab"></i></a></li>
                    </ul>

                    <!-- buttons -->
                    <div class="mt-4">
                        <a href="#contact" class="btn btn-default">Get in touch</a>
                    </div>
                </div>

                <!-- scroll down mouse wheel -->
                <div class="scroll-down light">
                    <a href="#about" class="mouse-wrapper">
                        <span>Scroll Down</span>
                        <span class="mouse">
                            <span class="wheel"></span>
                        </span>
                    </a>
                </div>

                <!-- parallax layers -->
                <div class="parallax" data-relative-input="true">

                    <svg width="27" height="29" data-depth="0.3" class="layer p1" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M21.15625.60099c4.37954 3.67487 6.46544 9.40612 5.47254 15.03526-.9929 5.62915-4.91339 10.30141-10.2846 12.25672-5.37122 1.9553-11.3776.89631-15.75715-2.77856l2.05692-2.45134c3.50315 2.93948 8.3087 3.78663 12.60572 2.22284 4.297-1.5638 7.43381-5.30209 8.22768-9.80537.79387-4.50328-.8749-9.08872-4.37803-12.02821L21.15625.60099z"
                            fill="#FFD15C" fill-rule="evenodd" />
                    </svg>

                    <svg width="26" height="26" data-depth="0.2" class="layer p2" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13 3.3541L2.42705 24.5h21.1459L13 3.3541z" stroke="#FF4C60" stroke-width="3"
                            fill="none" fill-rule="evenodd" />
                    </svg>

                    <svg width="30" height="25" data-depth="0.3" class="layer p3" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M.1436 8.9282C3.00213 3.97706 8.2841.92763 14.00013.92796c5.71605.00032 10.9981 3.04992 13.85641 8 2.8583 4.95007 2.8584 11.0491-.00014 16.00024l-2.77128-1.6c2.28651-3.96036 2.28631-8.84002.00011-12.8002-2.2862-3.96017-6.5124-6.40017-11.08513-6.4-4.57271.00018-8.79872 2.43984-11.08524 6.4002l-2.77128-1.6z"
                            fill="#44D7B6" fill-rule="evenodd" />
                    </svg>

                    <svg width="15" height="23" data-depth="0.6" class="layer p4" xmlns="http://www.w3.org/2000/svg">
                        <rect transform="rotate(30 9.86603 10.13397)" x="7" width="3" height="25" rx="1.5"
                            fill="#FFD15C" fill-rule="evenodd" />
                    </svg>

                    <svg width="15" height="23" data-depth="0.2" class="layer p5" xmlns="http://www.w3.org/2000/svg">
                        <rect transform="rotate(30 9.86603 10.13397)" x="7" width="3" height="25" rx="1.5"
                            fill="#6C6CE5" fill-rule="evenodd" />
                    </svg>

                    <svg width="49" height="17" data-depth="0.5" class="layer p6" xmlns="http://www.w3.org/2000/svg">
                        <g fill="#FF4C60" fill-rule="evenodd">
                            <path
                                d="M.5 16.5c0-5.71709 2.3825-10.99895 6.25-13.8567 3.8675-2.85774 8.6325-2.85774 12.5 0C23.1175 5.50106 25.5 10.78292 25.5 16.5H23c0-4.57303-1.90625-8.79884-5-11.08535-3.09375-2.28652-6.90625-2.28652-10 0C4.90625 7.70116 3 11.92697 3 16.5H.5z" />
                            <path
                                d="M23.5 16.5c0-5.71709 2.3825-10.99895 6.25-13.8567 3.8675-2.85774 8.6325-2.85774 12.5 0C46.1175 5.50106 48.5 10.78292 48.5 16.5H46c0-4.57303-1.90625-8.79884-5-11.08535-3.09375-2.28652-6.90625-2.28652-10 0-3.09375 2.28651-5 6.51232-5 11.08535h-2.5z" />
                        </g>
                    </svg>

                    <svg width="26" height="26" data-depth="0.4" class="layer p7" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13 22.6459L2.42705 1.5h21.1459L13 22.6459z" stroke="#FFD15C" stroke-width="3"
                            fill="none" fill-rule="evenodd" />
                    </svg>

                    <svg width="19" height="21" data-depth="0.3" class="layer p8" xmlns="http://www.w3.org/2000/svg">
                        <rect transform="rotate(-40 6.25252 10.12626)" x="7" width="3" height="25" rx="1.5"
                            fill="#6C6CE5" fill-rule="evenodd" />
                    </svg>

                    <svg width="30" height="25" data-depth="0.3" data-depth-y="-1.30" class="layer p9"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M29.8564 16.0718c-2.85854 4.95114-8.1405 8.00057-13.85654 8.00024-5.71605-.00032-10.9981-3.04992-13.85641-8-2.8583-4.95007-2.8584-11.0491.00014-16.00024l2.77128 1.6c-2.28651 3.96036-2.28631 8.84002-.00011 12.8002 2.2862 3.96017 6.5124 6.40017 11.08513 6.4 4.57271-.00018 8.79872-2.43984 11.08524-6.4002l2.77128 1.6z"
                            fill="#6C6CE5" fill-rule="evenodd" />
                    </svg>

                    <svg width="47" height="29" data-depth="0.2" class="layer p10" xmlns="http://www.w3.org/2000/svg">
                        <g fill="#44D7B6" fill-rule="evenodd">
                            <path
                                d="M46.78878 17.19094c-1.95535 5.3723-6.00068 9.52077-10.61234 10.8834-4.61167 1.36265-9.0893-.26708-11.74616-4.27524-2.65686-4.00817-3.08917-9.78636-1.13381-15.15866l2.34923.85505c-1.56407 4.29724-1.2181 8.92018.90705 12.12693 2.12514 3.20674 5.70772 4.5107 9.39692 3.4202 3.68921-1.0905 6.92581-4.40949 8.48988-8.70673l2.34923.85505z" />
                            <path
                                d="M25.17585 9.32448c-1.95535 5.3723-6.00068 9.52077-10.61234 10.8834-4.61167 1.36264-9.0893-.26708-11.74616-4.27525C.16049 11.92447-.27182 6.14628 1.68354.77398l2.34923.85505c-1.56407 4.29724-1.2181 8.92018.90705 12.12692 2.12514 3.20675 5.70772 4.5107 9.39692 3.4202 3.68921-1.0905 6.92581-4.40948 8.48988-8.70672l2.34923.85505z" />
                        </g>
                    </svg>

                    <svg width="33" height="20" data-depth="0.5" class="layer p11" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M32.36774.34317c.99276 5.63023-1.09332 11.3614-5.47227 15.03536-4.37895 3.67396-10.3855 4.73307-15.75693 2.77837C5.76711 16.2022 1.84665 11.53014.8539 5.8999l3.15139-.55567c.7941 4.50356 3.93083 8.24147 8.22772 9.8056 4.29688 1.56413 9.10275.71673 12.60554-2.2227C28.34133 9.98771 30.01045 5.4024 29.21635.89884l3.15139-.55567z"
                            fill="#FFD15C" fill-rule="evenodd" />
                    </svg>

                </div>
            </div>

        </section>

        <!-- section about -->
        <section id="about">

            <div class="container">

                <!-- section title -->
                <h2 class="section-title wow fadeInUp">About Me</h2>

                <div class="spacer" data-height="60"></div>

                <div class="row">

                    <div class="col-md-3">
                        <div class="text-center text-md-left">
                            <!-- avatar image -->
                            <img src="images/me.jpeg" alt="Me" class="w-75 circle shadow-dark" />
                        </div>
                        <div class="spacer d-md-none d-lg-none" data-height="30"></div>
                    </div>

                    <div class="col-md-9 triangle-left-md triangle-top-sm">
                        <div class="rounded bg-white shadow-dark padding-30">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- about text -->
                                    <p>I am Thomas Moreaux, a <?= round((time() - strtotime("1999-01-07")) / (60 * 60 * 24 * 365.25)) ?> years old Full Stack Developer from Paris suburbs, France. I am mostly specialized in web development, also I am pretty creative.
                                    </p>
                                    <div class="mt-3">
                                        <a href="https://www.linkedin.com/in/thomas-moreaux/" class="btn btn-default">Check my LinkedIn profile</a>
                                    </div>
                                    <div class="spacer d-md-none d-lg-none" data-height="30"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- row end -->

            </div>

        </section>

        <!-- section skills -->
        <section id="skills">

            <div class="container">

                <!-- section title -->
                <h2 class="section-title wow fadeInUp">Key skills</h2>

                <div class="spacer" data-height="60"></div>

                <div class="row">

                    <div class="col-md-4">
                        <!-- service box -->
                        <div class="service-box rounded data-background padding-30 text-center text-light shadow-blue"
                            data-color="#6C6CE5">
                            <span class="icon icon-star h1 mb-3 d-block"></span>
                            <h3 class="mb-4 mt-0">Expertise</h3>
                            <p class="mb-0"><h5 class="h6 d-inline-block">Languages & Tools :</h5>&nbsp;HTML, CSS, PHP, JavaScript, SQL, GIT.</p>
                            <p class="mb-0"><h5 class="h6 d-inline-block">Operating Systems :</h5>&nbsp;Windows, Linux/Debian/RHEL, macOS.</p>
                            <p class="mb-0"><h5 class="h6 d-inline-block">Softwares :</h5>&nbsp;Apache, Cordova, WordPress, PhpStorm/WebStorm.</p>
                        </div>
                        <div class="spacer d-md-none d-lg-none" data-height="30"></div>
                    </div>

                    <div class="col-md-4">
                        <!-- service box -->
                        <div class="service-box rounded data-background padding-30 text-center shadow-yellow"
                            data-color="#F9D74C">
                            <span class="icon icon-briefcase h1 mb-3 d-block"></span>
                            <h3 class="mb-3 mt-0">Good knowledge</h3>
                            <p class="mb-0"><h5 class="h6 d-inline-block">Languages & Tools :</h5>&nbsp;Python, C#, CPP, C, Java, Vue.JS.</p>
                            <p class="mb-0"><h5 class="h6 d-inline-block">Cloud computing :</h5>&nbsp;Scaleway Object Storage, AWS S3.</p>
                            <p class="mb-0"><h5 class="h6 d-inline-block">Softwares :</h5>&nbsp;Unity, Symfony, Suite Adobe, Office 365, Android Studio.</p>
                        </div>
                        <div class="spacer d-md-none d-lg-none" data-height="30"></div>
                    </div>

                    <div class="col-md-4">
                        <!-- service box -->
                        <div class="service-box rounded data-background padding-30 text-center text-light shadow-pink"
                            data-color="#F97B8B">
                            <span class="icon icon-puzzle h1 mb-3 d-block"></span>
                            <h3 class="mb-3 mt-0">Basic knowledge</h3>
                            <p class="mb-0"><h5 class="h6 d-inline-block">Languages & Tools :</h5>&nbsp;Docker, Kubernetes.</p>
                            <p class="mb-0"><h5 class="h6 d-inline-block">Operating Systems :</h5>&nbsp;Windows Server.</p>
                            <p class="mb-0"><h5 class="h6 d-inline-block">Cloud computing :</h5>&nbsp;Scaleway DBaaS, AWS EC2, Scaleway Containers.</p>
                        </div>
                    </div>

                </div>

                <div class="mt-5 text-center">
                    <p class="mb-0">What to learn more about my skills? <a href="#contact">Click here</a> to contact me! 👋</p>
                </div>

            </div>

        </section>

        <!-- section experience -->
        <section id="experience">

            <div class="container">

                <!-- section title -->
                <h2 class="section-title wow fadeInUp">Education & Experience</h2>

                <div class="spacer" data-height="60"></div>

                <div class="row">

                    <div class="col-md-6">

                        <!-- timeline wrapper -->
                        <div class="timeline edu bg-white rounded shadow-dark padding-30 overflow-hidden">

                            <!-- timeline item -->
                            <div class="timeline-container wow fadeInUp">
                                <div class="content">
                                    <span class="time">2020 - Present</span>
                                    <h3 class="title">Computer Science Engineering Degree</h3>
                                    <p>Apprenticeship study. "Cycle expert en ingéniérie informatique" Master degree at EPITA.
                                    </p>
                                </div>
                            </div>

                            <!-- timeline item -->
                            <div class="timeline-container wow fadeInUp" data-wow-delay="0.2s">
                                <div class="content">
                                    <span class="time">2019 - 2020</span>
                                    <h3 class="title">Computer Science Engineering Degree</h3>
                                    <p>Apprenticeship study. "Cycle ingénieur Informatique, objets connectés et sécurité" first year only (core curriculum) at ESILV.
                                    </p>
                                </div>
                            </div>

                            <!-- timeline item -->
                            <div class="timeline-container wow fadeInUp" data-wow-delay="0.4s">
                                <div class="content">
                                    <span class="time">2017 - 2019</span>
                                    <h3 class="title">MIP Technological University Degree</h3>
                                    <p>Multimedia and Internet Professions Degree ("Métiers du Multimédia et de l'Internet") at IUT de Vélizy.
                                    </p>
                                </div>
                            </div>

                            <!-- main line -->
                            <span class="line"></span>

                        </div>

                    </div>

                    <div class="col-md-6">

                        <!-- responsive spacer -->
                        <div class="spacer d-md-none d-lg-none" data-height="30"></div>

                        <!-- timeline wrapper -->
                        <div class="timeline exp bg-white rounded shadow-dark padding-30 overflow-hidden">

                            <!-- timeline item -->
                            <div class="timeline-container wow fadeInUp">
                                <div class="content">
                                    <span class="time">Sep. 2019 - Present</span>
                                    <h3 class="title">Full Stack Developer / Project Manager</h3>
                                    <p>Apprenticeship. Development and deployment in Agile mode of Web applications and management of Web projects at Enedis.
                                    </p>
                                </div>
                            </div>

                            <!-- timeline item -->
                            <div class="timeline-container wow fadeInUp" data-wow-delay="0.2s">
                                <div class="content">
                                    <span class="time">April 2019 - June 2019</span>
                                    <h3 class="title">Multimedia Developer</h3>
                                    <p>Internship. Creation of a multimedia pedagogical platform of scientific contents for first year students.
                                    </p>
                                </div>
                            </div>

                            <!-- timeline item -->
                            <div class="timeline-container wow fadeInUp" data-wow-delay="0.4s">
                                <div class="content">
                                    <span class="time">Nov. 2018 - Jan. 2019</span>
                                    <h3 class="title">Student contractor</h3>
                                    <p>Tutoring in computer science for first year students at IUT de Vélizy.
                                    </p>
                                </div>
                            </div>

                            <!-- main line -->
                            <span class="line"></span>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <!-- section works -->
        <section id="works">

            <div class="container">

                <!-- section title -->
                <h2 class="section-title wow fadeInUp">Recent works</h2>

                <div class="spacer" data-height="60"></div>

                <!-- portfolio filter (desktop) -->
                <ul class="portfolio-filter list-inline wow fadeInUp">
                    <li class="current list-inline-item" data-filter="*">Everything</li>
                    <li class="list-inline-item" data-filter=".dev">Dev</li>
                    <li class="list-inline-item" data-filter=".creative">Creative</li>
                    <li class="list-inline-item" data-filter=".writing">Writing</li>
                    <li class="list-inline-item" data-filter=".web">Web</li>
                </ul>

                <!-- portfolio filter (mobile) -->
                <div class="pf-filter-wrapper">
                    <select class="portfolio-filter-mobile">
                        <option value="*">Everything</option>
                        <option value=".dev">Dev</option>
                        <option value=".creative">Creative</option>
                        <option value=".writing">Writing</option>
                        <option value=".web">Web</option>
                    </select>
                </div>

                <!-- portolio wrapper -->
                <div class="row portfolio-wrapper">

                    <!-- portfolio item -->
                    <div class="col-md-4 col-sm-6 grid-item creative dev">
                        <a href="#cr-dialog" class="work-content">
                            <div class="portfolio-item rounded shadow-dark">
                                <div class="details">
                                    <span class="term">Dev, Creative</span>
                                    <h4 class="title">Crazy Racing - A playable ad inspired by toys racetracks</h4>
                                    <span class="more-button"><i class="icon-options"></i></span>
                                </div>
                                <div class="thumb">
                                    <img src="images/works/crazyracing.png" alt="Crazy Racing" />
                                    <div class="mask"></div>
                                </div>
                            </div>
                        </a>
                        <div id="cr-dialog" class="white-popup zoom-anim-dialog mfp-hide">
                            <img src="images/works/crazyracing.png" alt="Crazy Racing" />
                            <h2>Crazy Racing</h2>
                            <p>Crazy Racing is a playable ad made during a "game jam" week. Students from EPITA (Computer Science) and e-artsup (Game Design) were involve in this project of 4 people.</p>
                            <p>This PIXIjs game features automated racetrack generation, drift physics in turns, etc.</p>
                            <a href="crazyracing" target="_blank" class="btn btn-default">Play Now !</a>
                        </div>
                    </div>

                    <!-- portfolio item -->
                    <div class="col-md-4 col-sm-6 grid-item dev">
                        <a href="#bt-dialog" class="work-content">
                            <div class="portfolio-item rounded shadow-dark">
                                <div class="details">
                                    <span class="term">Dev</span>
                                    <h4 class="title">Bugtracker - A Composer package for GitLab issues</h4>
                                    <span class="more-button"><i class="icon-options"></i></span>
                                </div>
                                <div class="thumb">
                                    <img src="images/works/bugtracker.png" alt="Bugtracker" />
                                    <div class="mask"></div>
                                </div>
                            </div>
                        </a>
                        <div id="bt-dialog" class="white-popup zoom-anim-dialog mfp-hide">
                            <img src="images/works/bugtracker.png" alt="Bugtracker" />
                            <h2>Bugtracker</h2>
                            <p>Bugtracker is a work in progress composer package that allow users to send issues to gitlab directly from their website.</p>
                            <p>This project features a working GitLab CI/CD pipeline with unit tests (PHPUnit), code coverage (100% - published to GitLab Pages) and automated deployment to packagist (Composer repository).</p>
                            <a href="https://gitlab.com/thomas-mrx/bugtracker" target="_blank" class="btn btn-default">View on GitLab</a>
                        </div>
                    </div>

                    <!-- portfolio item -->
                    <div class="col-md-4 col-sm-6 grid-item writing">
                        <a href="#deepl-dialog" class="work-content">
                            <div class="portfolio-item rounded shadow-dark">
                                <div class="details">
                                    <span class="term">Writing</span>
                                    <h4 class="title">Deep Learning - An explanatory article (French)</h4>
                                    <span class="more-button"><i class="icon-options"></i></span>
                                </div>
                                <div class="thumb">
                                    <img src="images/works/deepl.png" alt="Deep Learning" />
                                    <div class="mask"></div>
                                </div>
                            </div>
                        </a>
                        <div id="deepl-dialog" class="white-popup zoom-anim-dialog mfp-hide">
                            <h2>Deep Learning - Article in French</h2>
                            <p>This article is a small university research project about Deep Learning.</p>
                            <p>It contains an introduction to the topic, a zoom on its origins, a explanatory part on its usages and a quick take on its future.</p>
                            <a href="deeplearning/MOREAUX_DeepLearning.pdf" target="_blank" class="btn btn-default">Download (.pdf)</a>
                        </div>
                    </div>

                    <!-- portfolio item -->
                    <div class="col-md-4 col-sm-6 grid-item creative dev web">
                        <a href="#mapaillasse-dialog" class="work-content">
                            <div class="portfolio-item rounded shadow-dark">
                                <div class="details">
                                    <span class="term">Dev, Web, Creative</span>
                                    <h4 class="title">maPaillasse - A pedagogical platform for students</h4>
                                    <span class="more-button"><i class="icon-options"></i></span>
                                </div>
                                <div class="thumb">
                                    <img src="images/works/mapaillasse.png" alt="maPaillasse" />
                                    <div class="mask"></div>
                                </div>
                            </div>
                        </a>
                        <div id="mapaillasse-dialog" class="white-popup zoom-anim-dialog mfp-hide">
                            <img src="images/works/mapaillasse.png" alt="maPaillasse" />
                            <h2>maPaillasse</h2>
                            <p>maPaillasse is a multimedia pedagogical platform of scientific contents for students. I developp this website from scratch (using jQuery and jQueryUI) and I made the scientific objects drawing in PhotoShop.</p>
                            <p>This project features an entire functional backend to create pedagogical content with an easy-to-use drag-and-drop editor.</p>
                            <a href="https://www.physique.uvsq.fr/physique-generale-lsph100#:~:text=de%20s%27auto%2D%C3%A9valuer.-,Travaux%20pratiques,-Une%20s%C3%A9ance%C2%A0de" target="_blank" class="btn btn-default">See it in action</a>
                        </div>
                    </div>

                    <!-- portfolio item -->
                    <div class="col-md-4 col-sm-6 grid-item dev">
                        <a href="#noztra-dialog" class="work-content">
                            <div class="portfolio-item rounded shadow-dark">
                                <div class="details">
                                    <span class="term">Dev</span>
                                    <h4 class="title">Noztra app - Discover 3D works in AR on the street</h4>
                                    <span class="more-button"><i class="icon-options"></i></span>
                                </div>
                                <div class="thumb">
                                    <img src="images/works/noztra.png" alt="Noztra" />
                                    <div class="mask"></div>
                                </div>
                            </div>
                        </a>
                        <div id="noztra-dialog" class="white-popup zoom-anim-dialog mfp-hide">
                            <img src="images/works/noztra.png" alt="Noztra" />
                            <h2>Noztra</h2>
                            <p>Noztra is a group project made during my 2 years Technological University Degree. We were working as a "Web agency" and I was the Lead Developer.</p>
                            <p>It is an application for discovering and collecting 3D artworks, for Android devices, with the aim of democratizing 3D art and introducing new artists.</p>
                        </div>
                    </div>


                    <!-- portfolio item -->
                    <div class="col-md-4 col-sm-6 grid-item web dev">
                        <a href="https://urgencesmods.fr" target="_blank">
                            <div class="portfolio-item rounded shadow-dark">
                                <div class="details">
                                    <span class="term">Dev, Web</span>
                                    <h4 class="title">UrgencesMods.fr - A GTA 5 mods sharing website</h4>
                                    <span class="more-button"><i class="icon-link"></i></span>
                                </div>
                                <div class="thumb">
                                    <img src="images/works/urgencesmods.png" alt="Portfolio-title" />
                                    <div class="mask"></div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>

            </div>

        </section>

        <!-- section contact -->
        <section id="contact">

            <div class="container">

                <!-- section title -->
                <h2 class="section-title wow fadeInUp">Get In Touch</h2>

                <div class="spacer" data-height="60"></div>

                <div class="row">

                    <div class="col-md-4">
                        <!-- contact info -->
                        <div class="contact-info">
                            <h3 class="wow fadeInUp">Let's talk about everything!</h3>
                            <p class="wow fadeInUp">Don't like forms? Send me an <a
                                    href="mailto:thomas@moreaux.dev">email</a>. 👋</p>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <!-- Contact Form -->
                        <form id="contact-form" class="contact-form mt-6" method="post" action="form/contact.php">

                            <div class="messages"></div>

                            <div class="row">
                                <div class="column col-md-6">
                                    <!-- Name input -->
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="InputName" id="InputName"
                                            placeholder="Your name" required="required" data-error="Name is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="column col-md-6">
                                    <!-- Email input -->
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="InputEmail" name="InputEmail"
                                            placeholder="Email address" required="required"
                                            data-error="Email is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="column col-md-12">
                                    <!-- Email input -->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="InputSubject" name="InputSubject"
                                            placeholder="Subject" required="required" data-error="Subject is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="column col-md-12">
                                    <!-- Message textarea -->
                                    <div class="form-group">
                                        <textarea name="InputMessage" id="InputMessage" class="form-control" rows="5"
                                            placeholder="Message" required="required"
                                            data-error="Message is required."></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" name="submit" id="submit" value="Submit" class="btn btn-default">Send
                                Message</button><!-- Send Button -->

                        </form>
                        <!-- Contact Form end -->
                    </div>

                </div>

            </div>

        </section>

        <div class="spacer" data-height="96"></div>

    </main>

    <!-- Go to top button -->
    <a href="javascript:" id="return-to-top"><i class="fas fa-arrow-up"></i></a>

    <!-- SCRIPTS -->
    <script src="js/jquery-1.12.3.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/infinite-scroll.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/contact.js"></script>
    <script src="js/validator.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/morphext.min.js"></script>
    <script src="js/parallax.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/custom.js"></script>

</body>

</html>
